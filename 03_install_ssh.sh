#!/usr/bin/env bash

apt-get install openssh-server

echo "Change: Port 2222"
echo "Change: UsePrivelegeSeparation no"
read -p "Press Enter to start"
nano /etc/ssh/sshd_config

mkdir /root/.ssh
cat >/root/.ssh/authorized_keys <<EOL
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC5qC8cVXlTu+4MuAD/lMLCP/I08X1v0SI0M0kVBx3rCqY3TGYLxKmDEAUrS+BiO4y5TAB3u3u7RszOM9XCw3+Hf5zmoElkkt4Z9IMCtnqJJbBeyxcQWJ5+74eNst6DRi6P5gjxuM2PEr0sFWV36AF0S1XLVHEY+ofVvycFy3HEcuzU+2ETEcY1rNYrg73VDgH4ie3BipM/rDoZRup0/HMhEi3FUdorVyk6ew48JbgOgyW8nFsNdNc41CJCURhUIy5CwjuOtvnh55MSRQhGiCbxI8nETH2Duf4XvksAgVnTjed6l/d5Unpkr4XrbyPi66YOzNi7DFgUD+bOQsOuXM4l rudenko@volidar.com
EOL

service ssh restart

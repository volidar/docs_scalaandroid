#!/usr/bin/env bash


#Unpack
cd /opt
tar -xzvf /opt/sd/tmp/scala-2.11.7.tgz 
ln -s scala-2.11.7 scala


#Configure
read -p "Add export JAVA_HOME=/opt/jdk"
nano /opt/scala/bin/scala

read -p "Add export JAVA_HOME=/opt/jdk"
nano /opt/scala/bin/scalac

cd /usr/bin
ln -s /opt/scala/bin/scala
ln -s /opt/scala/bin/scalac


#Optimize
mkdir /opt/sd/opt/scala
cd /opt/scala
mv lib /opt/sd/opt/scala/
ln -s /opt/sd/opt/scala/lib

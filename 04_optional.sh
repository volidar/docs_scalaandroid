#!/usr/bin/env bash

apt-get install dialog net-tools man
read -p "Press Enter and choose your Time-Zone"
dpkg-reconfigure tzdata

read -p "Press Enter and uncomment: precedence ::ffff:0:0/96  100 #Prefer IP4"
nano /etc/gai.conf
	
cat >>/etc/profile <<EOL
alias ll='ls $LS_OPTIONS -l'
alias l='ls $LS_OPTIONS -lA'
alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'
alias scr='screen -d -R main'
EOL

read -p "Press Enter and check aliases in the end"
nano /etc/profile

echo "You are ready to reconnect over SSH"

#!/usr/bin/env bash

cd /opt/sd/tmp
#JDK
wget --no-check-certificate --no-cookies --header "Cookie: oraclelicense=accept-securebackup-cookie" http://download.oracle.com/otn-pub/java/jdk/8u65-b17/jdk-8u65-linux-arm32-vfp-hflt.tar.gz
wget http://downloads.typesafe.com/scala/2.11.7/scala-2.11.7.tgz
wget https://downloads.typesafe.com/typesafe-activator/1.3.7/typesafe-activator-1.3.7-minimal.zip



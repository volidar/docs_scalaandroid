#!/usr/bin/env bash


#Unpack
cd /opt
tar -xzvf /opt/sd/tmp/jdk-8u65-linux-arm32-vfp-hflt.tar.gz
ln -s /opt/jdk1.8.0_65 jdk


#Optimize
cd /opt/jdk
rm -Rf man src.zip
rm -Rf db include
rm -Rf COPYRIGHT LICENSE README.html THIRDPARTYLICENSEREADME.txt jre/COPYRIGHT jre/LICENSE jre/README jre/THIRDPARTYLICENSEREADME.txt jre/Welcome.html

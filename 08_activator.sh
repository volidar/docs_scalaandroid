#!/usr/bin/env bash


#Unpack
cd /opt
unzip /opt/sd/tmp/typesafe-activator-1.3.7-minimal.zip
ln -s activator-1.3.7-minimal activator


#Configure
echo "Add: export JAVA_HOME=/opt/jdk"
echo '     export _JAVA_OPTIONS="-Xms192M -Xmx192M"'
read -p "Press Enter"
nano /opt/activator/activator


cd /usr/bin
ln -s /opt/activator/activator


#Optimize
cd /opt/activator
rm -f activator.bat

cd /opt/sd/opt/
activator new hello-scala hello-scala

mkdir -p /opt/sd/opt/ivy2
cd /root/.ivy2
cp -RL cache /opt/sd/opt/ivy2/
rm -Rf cache
ln -s /opt/sd/opt/ivy2/cache

mkdir -p /opt/sd/opt/sbt
cd /root/.sbt
cp -RL boot /opt/sd/opt/sbt/
rm -Rf boot
ln -s /opt/sd/opt/sbt/boot

cd hello-scala
nice -n 19 activator run
